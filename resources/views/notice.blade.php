@extends('custom')

@section('content')

<div class="container">

    <h3> Notice Board </h3>
    <hr>

    <ol>
        @foreach($noticeAllData as $key=>$value)
        <li> <a href="/notice/{!! $value->notice_id !!}">{!! $value->notice_name !!}</a></li> <br>
        @endforeach
    </ol>

    @endsection

</div>