<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Faculty Profile</title>


    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    .container{

        width: 1350px;
    }

    .header{
        height: 200px;
        background: white; /* For browsers that do not support gradients */
        background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
        background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
        background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
        background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
    }
    .navbar-default{
        background: linear-gradient(white ,#afd9ee , #afd9ee);
    }
    .nopadding{
        padding: 0;
        margin-top: 30px;
        background: linear-gradient(white ,white , #afd9ee);
    }
    .nopadding h4{
        background: linear-gradient( #afd9ee,white , #afd9ee);
        padding: 10px 15px;  
    }

    .nopadding ul li{

        background: url("/public/images/arrow.jpg") no-repeat left -1.6rem;
        padding-left: 10px;
        background-size: 25px 35px;
        margin: 15px 17px;
    }




    
</style>
</head>
<body>

<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="/home1.blade.php">Home</a></li>
                    <li><a href="/admission.blade.php">Course Info</a></li>
                    <li class="active"><a href="/faculty">Faculty Members</a></li>
                    <li><a href="/notice.blade.php">Notice Board</a></li>

                    <li><a href="/class_routine.blade.php">Class Routine</a></li>
                    <li><a href="/research.blade.php">Research</a></li>
                    <li><a href="/contact.blade.php">Contact Info</a></li>
                </ul>

                </ul>
            </div>
        </nav>
    </div>

    <div class="col-lg-10" style="margin-left: 100px">
    
    @foreach($facultyData as $key=>$value)
        <div class="col-md-4">
            <img src="data:image/jpeg;base64,{!!$value->t_image!!}" style="width: 300px;height: 300px;margin-top: 20px">
        </div>
        <div class="col-md-8">
            <h1 style="margin-bottom: 0">{!!$value->t_name!!}</h1>
            <br>
            <h2 style="margin: 0;padding: 0;">{!!$value->b_name!!}</h2><hr>
            <h4>{!!$value->t_designation!!}</h4>
            <b>E-mail:</b>           {!!$value->t_email!!}  <br><b>Contact:</b> {!!$value->contact!!}<hr>
            <b>Research Interests:</b>
                {!!$value->r_interest!!}
            <hr>
        </div>

        <div class="col-sm-12 nopadding">
            <h4><b>Education :</b></h4>
            <ul>
                <li><b>B.Sc.</b><br>{!!$value->b_sub!!}<br>{!!$value->b_uni!!}<br>{!!$value->b_country!!}</li>
                <li><b>M.Sc.</b><br>{!!$value->m_sub!!}<br>{!!$value->m_uni!!}<br>{!!$value->m_country!!}</li>
                <li><b>PhD</b><br>{!!$value->p_sub!!}<br>{!!$value->p_uni!!}</li>
            </ul>
        </div>
        @endforeach
        
                     
           <div class="col-sm-12 nopadding" style="background: linear-gradient( white ,#afd9ee, #afd9ee);">
            <h4><b>Publications :</b></h4>
            <h5 align="center"><b>Journals </b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
            @if(isset($retInfo['journal']))
            @foreach($retInfo['journal'] as $value=>$datas)
                <li>{!!$datas['author_name']!!},"{!!$datas['paper_name']!!}",{!!$datas['journal_name']!!},{!!$datas['publication_year']!!}</li>
             @endforeach
             @endif    
            </ul>


            <hr>
            <h5 align="center"><b>Conferences</b></h5>
            <hr style="border-bottom-color: blue">
            <ul>
                @if(isset($retInfo['conference']))
            @foreach($retInfo['conference'] as $value=>$datas)
                <li>{!!$datas['author_name']!!},"{!!$datas['paper_name']!!}",{!!$datas['journal_name']!!},{!!$datas['publication_year']!!}</li>

             @endforeach
                    <br> <br> <br> <br>
             @endif    
            </ul>

        </div>


	


    </div>


</div>

<div class="footer" style="height:50px; background: linear-gradient(white ,#afd9ee , #afd9ee)">
    <br>
    <center><b style="color: white"> Copyright &#169; Department of CSE, CUET| 2017</b> </center>

</div>



</body>
</html>
