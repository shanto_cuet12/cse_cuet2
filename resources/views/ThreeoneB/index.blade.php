@extends('../layouts/app')
<head>

    <style>

        td{

            width:100px;
            height:50px


        }

        .container{
            position: absolute;
            left: 50px;
            

        }


    </style>

</head>


@section('content')

<div class="container">

    

        <a href="create"><button type="button" class="btn btn-primary btn-lg">Add New</button></a>
    <br>





    Total: {!! $allData->total() !!} Record(s) <br>

    Showing: {!! $allData->count() !!} Record(s) <br>

    {!! $allData->links() !!}




    <table class="table table-bordered table table-striped" >

        <th>Subject Code</th>
        <th>Teacher Name</th>
        <th>Day</th>
        <th>Time</th>

        <th>Action Buttons</th>

        @foreach($allData as $oneData)

            <tr>

                <td>  {!! $oneData['sub_title'] !!} </td>
                <td>  {!! $oneData['teacher_name'] !!} </td>
                <td>  {!! $oneData['day'] !!} </td>
                <td>  {!! $oneData['time'] !!} </td>


                <td>
                    <a href="edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                    <a href="delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                </td>

            </tr>


        @endforeach


    </table>
    {!! $allData->links() !!}
</div>
@endsection
