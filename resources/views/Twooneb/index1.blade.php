<!DOCTYPE html>
<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 15px;
        }
    </style>
</head>
<body>

<table style="width:100%">
    <tr>
        <th>Day/Time</th>
        <th>9.00-9.50AM</th>
        <th>9.50-10.40AM</th>
        <td rowspan="6"> B<br> R <br> E <br> A <br> K <br> </td>
        <th>11.00AM-11.50AM</th>
        <th>11.50AM-12.40PM</th>
        <th>12.40PM-1.30PM</th>
        <td rowspan="6"> B<br> R <br> E <br> A <br> K <br> </td>
        <th>2.30PM-3.20PM</th>
        <th>3.20PM-4.10PM</th>
        <th>4.10PM-5.00PM</th>

    </tr>


    <tr>
        <td><strong>Sunday</strong></td>
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Sunday' && $oneData['time']=='9.00 a.m-9.50 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>

            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Sunday' && $oneData['time']=='9.50 a.m-10.40 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Sunday' && $oneData['time']=='11.00 a.m-11.50 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Sunday' && $oneData['time']=='11.50 a.m-12.40 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Sunday' && $oneData['time']=='12.40 p.m-1.30 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Sunday' && $oneData['time']=='2.30 p.m-3.20 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Sunday' && $oneData['time']=='3.20 p.m-4.10 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Sunday' && $oneData['time']=='4.10 p.m-5.00 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

    </tr>


    <tr>
        <td><strong>Monday</strong></td>
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Monday' && $oneData['time']=='9.00 a.m-9.50 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>

            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Monday' && $oneData['time']=='9.50 a.m-10.40 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Monday' && $oneData['time']=='11.00 a.m-11.50 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Monday' && $oneData['time']=='11.50 a.m-12.40 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Monday' && $oneData['time']=='12.40 p.m-1.30 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Monday' && $oneData['time']=='2.30 p.m-3.20 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Monday' && $oneData['time']=='3.20 p.m-4.10 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Monday' && $oneData['time']=='4.10 p.m-5.00 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

    </tr>

    <tr>
        <td><strong>Tuesday</strong></td>
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Tuesday' && $oneData['time']=='9.00 a.m-9.50 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>

            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Tuesday' && $oneData['time']=='9.50 a.m-10.40 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Tuesday' && $oneData['time']=='11.00 a.m-11.50 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Tuesday' && $oneData['time']=='11.50 a.m-12.40 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Tuesday' && $oneData['time']=='12.40 p.m-1.30 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Tuesday' && $oneData['time']=='2.30 p.m-3.20 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Tuesday' && $oneData['time']=='3.20 p.m-4.10 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Tuesday' && $oneData['time']=='4.10 p.m-5.00 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

    </tr>

    <tr>
        <td><strong>Wednesday</strong></td>
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Wednesday' && $oneData['time']=='9.00 a.m-9.50 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>

            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Wednesday' && $oneData['time']=='9.50 a.m-10.40 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Wednesday' && $oneData['time']=='11.00 a.m-11.50 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Wednesday' && $oneData['time']=='11.50 a.m-12.40 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Wednesday' && $oneData['time']=='12.40 p.m-1.30 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Wednesday' && $oneData['time']=='2.30 p.m-3.20 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Wednesday' && $oneData['time']=='3.20 p.m-4.10 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Wednesday' && $oneData['time']=='4.10 p.m-5.00 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

    </tr>

    <tr>
        <td><strong>Thursday</strong></td>
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Thursday' && $oneData['time']=='9.00 a.m-9.50 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>

            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Thursday' && $oneData['time']=='9.50 a.m-10.40 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Thursday' && $oneData['time']=='11.00 a.m-11.50 a.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Thursday' && $oneData['time']=='11.50 a.m-12.40 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Thursday' && $oneData['time']=='12.40 p.m-1.30 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Thursday' && $oneData['time']=='2.30 p.m-3.20 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Thursday' && $oneData['time']=='3.20 p.m-4.10 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach
        <?php $i=0;?>
        @foreach($allData as $oneData)
            @if($oneData['day']=='Thursday' && $oneData['time']=='4.10 p.m-5.00 p.m')
                <td> {!! $oneData['sub_title'] !!}<br>{!! $oneData['teacher_name'] !!}</td>
            @elseif(sizeof($allData)===$i+1)
                <td> None</td>
            @else
                <?php $i=$i+1;?>
            @endif
        @endforeach

    </tr>

</table>



</body>
</html>
