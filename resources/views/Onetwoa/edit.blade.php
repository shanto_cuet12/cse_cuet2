
@extends('../layouts/app')

@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Level 1 Term 2 Routine(Section A)</h3>
            <hr>

            {!! Form::open(['url'=>'/Onetwoa/update']) !!}

            {!! Form::label('sub_title','Subject Code Title:') !!}
            {!! Form::text('sub_title',$oneData['sub_title'],['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('teacher_name','Teacher Name:') !!}
            {{ Form::select('teacher_name', [
            'Please Select one',
               'Dr. MD Samsul Arefin' => 'Dr. MD Samsul Arefin',
               'Dr. Md. Ibrahim Khan' => 'Dr. Md. Ibrahim Khan',
               'Dr. Kaushik Deb' => 'Dr. Kaushik Deb',
               'Dr. Mohammed Moshiul Hoque' => 'Dr. Mohammed Moshiul Hoque',
               'Dr. Asaduzzaman' => 'Dr. Asaduzzaman',
               'Dr. Pranab Kumar Dhar' => 'Dr. Pranab Kumar Dhar',
               'Dr. Md. Mokammel Haque' => 'Dr. Md. Mokammel Haque',
               'Abu Hasnat Mohammad Ashfak Habib' => 'Abu Hasnat Mohammad Ashfak Habib',
               'Mohammad Obaidur Rahman' => 'Mohammad Obaidur Rahman',
                'Mir Md. Saki Kowsar' => 'Mir Md. Saki Kowsar',
                'Muhammad Kamal Hossen' => 'Muhammad Kamal Hossen',
                'Md. Iqbal Hasan Sarker'=> 'Md. Iqbal Hasan Sarker',
                'Md. Monjur-Ul-Hasan'=> 'Md. Monjur-Ul-Hasan',
                 'Rahma Bintey Mufiz Mukta' => 'Rahma Bintey Mufiz Mukta',
                 'Lamia Alam' => 'Lamia Alam',
                'Shayla Sharmin' => 'Shayla Sharmin',
                'Md. Sabir Hossain' => 'Md. Sabir Hossain',
                'Farzana Yasmin' => 'Farzana Yasmin',
               'Animesh Chandra Roy' => 'Animesh Chandra Roy',
               'Md. Shafiul Alam Forha' => 'Md. Shafiul Alam Forhad',
               'Jibon Naher' => 'Jibon Naher',
               'Sharmistha Chanda Tista' => 'Sharmistha Chanda Tista',
               'Tanzina Akter' => 'Tanzina Akter']
            ) }}
            <br>
            {!! Form::label('day',' Day:') !!}
            {{ Form::select('day',['Please select one',
   'Sunday' => 'Sunday',
   'Monday' => 'Monday',
   'Tuesday' => 'Tuesday',
   'Wednesday' => 'Wednesday',
   'Thursday' => 'Thursday']
) }}
            {!! Form::label('time',' Time:') !!}
            {{ Form::select('time',  ['Please select one',
   '9.00 a.m-9.50 a.m' => '9.00-9.50',
   '9.50 a.m-10.40 a.m' => '9.50-10.40',
   '11.00 a.m-11.50 a.m' => '11.00-11.50',
   '11.50 a.m-12.40 p.m' => '11.50-12.40',
   '12.40 p.m-1.30 p.m' => '12.40-1.30',
   '2.30 p.m-3.20 p.m' => '2.30-3.20',
   '3.20 p.m-4.10 p.m' => '3.20-4.10',
   '4.10 p.m-5.00 p.m' => '4.10-5.00',
   '9.00 am - 10.40 am' => '9.00 am - 10.40 am',
   '11.00 am - 1.30 pm' => '11.00 am - 1.30 pm',
   '2.30 pm - 5.00 pm' => '2.30 pm - 5.00 pm']
) }}
            <br>

            {!! Form::text('id',$oneData['id'],['hidden'=>'hidden' ]) !!}


            {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}
            <br><br>

            {!! Form::close() !!}

        </div>
    </div>

@endsection