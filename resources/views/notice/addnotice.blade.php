
@extends('../custom')

@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Notice Upoload Form</h3>
            <hr>

            {!! Form::open(['url'=>'notice/store', 'files' => true]) !!}

            {!! Form::label('notice_name','Notice Name:') !!}
            {!! Form::text('notice_name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('image','Upload Notice:') !!}
            {!! Form::file('image',['class'=>'form-control', 'required'=>'required']) !!}


            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            <br> <br> <br> <br>
            {!! Form::close() !!}

        </div>

    </div>

@endsection
