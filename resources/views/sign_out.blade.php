@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @foreach($teacher_datas as $key=> $value)
                            <h3> {!! $value->t_name !!} <br> </h3>
                            <i> <b> {!! $value->t_designation!!} </b> </i>
                        @endforeach

                        <br>
                        {{-- email: {!!$email !!} <br>
                          status: {!!$checker  !!}

--}}

                        <table class="table table-bordered table table-striped" >

                            <tr>
                                <th colspan="5"; style="width: 25px; background-color: whitesmoke"> Sunday</th>
                            </tr>
                            <tr>
                                <th style="width: 50px; background-color: white">Time</th>
                                <th style="width: 50px; background-color: white">Course No</th>
                                <th style="width: 50px; background-color: white">Level</th>
                                <th style="width: 50px; background-color: white">Term</th>
                                <th style="width: 50px; background-color: white"> Section</th>

                            </tr>

                            @if(isset($schedule_data))
                                <?php $i=0;?>
                                @foreach($schedule_data as $key=> $value)
                                    <tr>
                                        @if($value['Day']=='Sunday')
                                            <td> {!! $value['Time'] !!} </td>
                                            <td> {!! $value['Course'] !!} </td>
                                            <td> {!! $value['Level'] !!} </td>
                                            <td> {!! $value['Term'] !!} </td>
                                            <td> {!! $value['Section'] !!} </td>
                                        @elseif(sizeof($schedule_data)===$i+1)
                                            <td colspan="5"> No Class</td>
                                        @else
                                            <?php $i=$i+1;?>
                                        @endif
                                    </tr>
                                    {{--{!! $value->rank !!}<br>--}}
                                @endforeach
                            @endif

                        </table>

                        <table class="table table-bordered table table-striped" >

                            <tr>
                                <th colspan="5"; style="width: 25px; background-color: whitesmoke"> Monday</th>
                            </tr>
                            <tr>
                                <th style="width: 50px; background-color: white">Time</th>
                                <th style="width: 50px; background-color: white">Course No</th>
                                <th style="width: 50px; background-color: white">Level</th>
                                <th style="width: 50px; background-color: white">Term</th>
                                <th style="width: 50px; background-color: white"> Section</th>

                            </tr>

                            @if(isset($schedule_data))
                                <?php $i=0;?>
                                @foreach($schedule_data as $key=> $value)
                                    <tr>
                                        @if($value['Day']=='Monday')
                                            <td> {!! $value['Time'] !!} </td>
                                            <td> {!! $value['Course'] !!} </td>
                                            <td> {!! $value['Level'] !!} </td>
                                            <td> {!! $value['Term'] !!} </td>
                                            <td> {!! $value['Section'] !!} </td>
                                        @elseif(sizeof($schedule_data)===$i+1)
                                            <td colspan="5"> No Class</td>
                                        @else
                                            <?php $i=$i+1;?>
                                        @endif
                                    </tr>
                                    {{--{!! $value->rank !!}<br>--}}
                                @endforeach
                            @endif

                        </table>

                        <table class="table table-bordered table table-striped" >

                            <tr>
                                <th colspan="5"; style="width: 25px; background-color: whitesmoke"> Tuesday</th>
                            </tr>
                            <tr>
                                <th style="width: 50px; background-color: white">Time</th>
                                <th style="width: 50px; background-color: white">Course No</th>
                                <th style="width: 50px; background-color: white">Level</th>
                                <th style="width: 50px; background-color: white">Term</th>
                                <th style="width: 50px; background-color: white"> Section</th>

                            </tr>

                            @if(isset($schedule_data))
                                <?php $i=0;?>
                                @foreach($schedule_data as $key=> $value)
                                    <tr>
                                        @if($value['Day']=='Tuesday')
                                            <td> {!! $value['Time'] !!} </td>
                                            <td> {!! $value['Course'] !!} </td>
                                            <td> {!! $value['Level'] !!} </td>
                                            <td> {!! $value['Term'] !!} </td>
                                            <td> {!! $value['Section'] !!} </td>
                                        @elseif(sizeof($schedule_data)===$i+1)
                                            <td colspan="5"> No Class</td>
                                        @else
                                            <?php $i=$i+1;?>
                                        @endif
                                    </tr>
                                    {{--{!! $value->rank !!}<br>--}}
                                @endforeach
                            @endif

                        </table>

                        <table class="table table-bordered table table-striped" >

                            <tr>
                                <th colspan="5"; style="width: 25px; background-color: whitesmoke"> Wednesday</th>
                            </tr>
                            <tr>
                                <th style="width: 50px; background-color: white">Time</th>
                                <th style="width: 50px; background-color: white">Course No</th>
                                <th style="width: 50px; background-color: white">Level</th>
                                <th style="width: 50px; background-color: white">Term</th>
                                <th style="width: 50px; background-color: white"> Section</th>

                            </tr>

                            @if(isset($schedule_data))
                                <?php $i=0;?>
                                @foreach($schedule_data as $key=> $value)
                                    <tr>
                                        @if($value['Day']=='Wednesday')
                                            <td> {!! $value['Time'] !!} </td>
                                            <td> {!! $value['Course'] !!} </td>
                                            <td> {!! $value['Level'] !!} </td>
                                            <td> {!! $value['Term'] !!} </td>
                                            <td> {!! $value['Section'] !!} </td>
                                        @elseif(sizeof($schedule_data)===$i+1)
                                            <td colspan="5"> No Class</td>
                                        @else
                                            <?php $i=$i+1;?>
                                        @endif
                                    </tr>
                                    {{--{!! $value->rank !!}<br>--}}
                                @endforeach
                            @endif

                        </table>

                        <table class="table table-bordered table table-striped" >

                            <tr>
                                <th colspan="5"; style="width: 25px; background-color: whitesmoke"> Thursday</th>
                            </tr>
                            <tr>
                                <th style="width: 50px; background-color: white">Time</th>
                                <th style="width: 50px; background-color: white">Course No</th>
                                <th style="width: 50px; background-color: white">Level</th>
                                <th style="width: 50px; background-color: white">Term</th>
                                <th style="width: 50px; background-color: white"> Section</th>

                            </tr>

                            @if(isset($schedule_data))
                                <?php $i=0;?>
                                @foreach($schedule_data as $key=> $value)
                                    <tr>
                                        @if($value['Day']=='Thursday')
                                            <td> {!! $value['Time'] !!} </td>
                                            <td> {!! $value['Course'] !!} </td>
                                            <td> {!! $value['Level'] !!} </td>
                                            <td> {!! $value['Term'] !!} </td>
                                            <td> {!! $value['Section'] !!} </td>
                                        @elseif(sizeof($schedule_data)===$i+1)
                                            <td colspan="5"> No Class</td>
                                        @else
                                            <?php $i=$i+1;?>
                                        @endif
                                    </tr>
                                    {{--{!! $value->rank !!}<br>--}}
                                @endforeach
                            @endif

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6" style="height: 50px; background-color: #2ca02c; margin-left: 310px; width: 750px;">
        <center><a href="Research/create"><button type="button" class="btn btn-primary" style="margin-top: 5px;">Add new paper</button></a></center>


    </div>
@endsection
