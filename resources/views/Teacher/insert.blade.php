
@extends('../custom')

@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Faculty Member Registration</h3>
            <hr>

            {!! Form::open(['url'=>'/TeacherRegistrationS', 'files' => true]) !!}

            {!! Form::label('t_name','Name(in English):') !!}
            {!! Form::text('t_name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>
            {!! Form::label('t_email','Email:') !!}
            {!! Form::email('t_email','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>


            {!! Form::label('t_designation','Designation:') !!}
            {!! Form::text('t_designation','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>
            {!! Form::label('b_name','Name(in Bangla):') !!}
            {!! Form::text('b_name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>


            {!! Form::label('contact','Contact No.:') !!}
            {!! Form::text('contact','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>
            {!! Form::label('image','Picture:') !!}
            {!! Form::file('image','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('r_interest','Research Interest:') !!}
            {!! Form::text('r_interest','',['class'=>'form-control']) !!}

            <br>
            <b>B.SC</b>
            <hr>

            {!! Form::label('b_sub','Subject:') !!}
            {!! Form::text('b_sub','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('b_uni','University:') !!}
            {!! Form::text('b_uni','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>
            {!! Form::label('b_country','Country:') !!}
            {!! Form::text('b_country','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            <b>M.SC</b>
            <hr>

            {!! Form::label('m_sub','Subject:') !!}
            {!! Form::text('m_sub','',['class'=>'form-control']) !!}

            <br>

            {!! Form::label('m_uni','University:') !!}
            {!! Form::text('m_uni','',['class'=>'form-control']) !!}

            <br>

            {!! Form::label('m_country','Country:') !!}
            {!! Form::text('m_country','',['class'=>'form-control']) !!}

            <br>

            <b>Phd</b>
            <hr>

            {!! Form::label('p_sub','Subject:') !!}
            {!! Form::text('p_sub','',['class'=>'form-control']) !!}

            <br>


            {!! Form::label('p_uni','University/Country:') !!}
            {!! Form::text('p_uni','',['class'=>'form-control']) !!}

            <br>
            <b>Password</b>
            <hr>

			{!! Form::label('password','Password:') !!} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            {!! Form::password('password','',['class'=>'form-control', 'required'=>'required']) !!}

            <br><br>

			{!! Form::label('password_confirmation','Confirm Password:') !!} &nbsp;
            {!! Form::password('password_confirmation','',['class'=>'form-control', 'required'=>'required']) !!}


            {{--{!! Form::label('rcp','Related courses(postgraduate):') !!}--}}
            {{--{!! Form::text('rcp','',['class'=>'form-control', 'required'=>'required']) !!}--}}

            {{--<br>--}}

            {{--{!! Form::label('rcu','Related courses(undergraduate):') !!}--}}
            {{--{!! Form::text('rcu','',['class'=>'form-control', 'required'=>'required']) !!}--}}

            {{--<br>--}}

            {{--{!! Form::label('review','Reviwer:') !!}--}}
            {{--{!! Form::text('review','',['class'=>'form-control', 'required'=>'required']) !!}--}}

            {{--<br>--}}

            {{--{!! Form::label('password','Password:') !!}--}}
            {{--{!! Form::password('password','',['class'=>'form-control', 'required'=>'required']) !!}--}}

            {{--<br>--}}
			{{--{!! Form::label('password_confirmation','Confirm Password:') !!}--}}
            {{--{!! Form::password('password_confirmation','',['class'=>'form-control', 'required'=>'required']) !!}--}}

            {{--<br>--}}
            <br> <br> <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}
            <br> <br> <br>

            {!! Form::close() !!}

        </div>

    </div>

@endsection
