<?php

namespace App\Http\Controllers;

use App\Onetwoa;
use Illuminate\Http\Request;

class OnetwoaController extends Controller
{
    public function store(){
        $objOnetwoa=new Onetwoa();
        $objOnetwoa->sub_title=$_POST['sub_title'];
        $objOnetwoa->teacher_name=$_POST['teacher_name'];
        $objOnetwoa->day=$_POST['day'];
        $objOnetwoa->time=$_POST['time'];
        $status=$objOnetwoa->save();
        return redirect()->route('Onetwoaroutine');
    }
    public function index(){
        $objOnetwoa=new Onetwoa();
        $allData=$objOnetwoa->paginate(20);
        return view("Onetwoa/index",compact('allData'));
    }
    public function index1(){
        $objOnetwoa=new Onetwoa();
        $allData=$objOnetwoa->paginate(20);
        return view("Onetwoa/index1",compact('allData'));
    }
    public function view4Edit($id){
        $objOnetwoa=new Onetwoa();
        $oneData=$objOnetwoa->find($id);
        return view("Onetwoa/edit",compact('oneData'));
    }

    public function update(){
        $objOnetwoa=new Onetwoa();
        $oneData=$objOnetwoa->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('Onetwoaroutine');
    }
    public function delete($id){
        $objOnetwoa=new Onetwoa();
        $oneData=$objOnetwoa->find($id)->delete();
        return redirect()->route('Onetwoaroutine');
    }
}
