<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Validator;

class noticeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('notice/addnotice');
	}

	public  function store(Request $request){
        $this->validator($request->all());
        if ($request->hasFile('image')){
            $destinationPath=public_path('images\notice');
            $file     = request()->file('image');
            $fileName = 'notice'.date('_j\_m_Y_').rand(1,15).'_'. $file->getClientOriginalName();
            $notice_name=$request['notice_name'];

            if(request()->file('image')->move($destinationPath, $fileName)){
                DB::select('INSERT INTO notice (notice_id, notice_name, notice_path) VALUES (NULL, "'.$notice_name.'","'.$fileName.'")');
                 return   redirect('/sign_out');
            }
            else
                return   redirect('/notice/addnotice');
        }
        else
            return   redirect('/notice/addnotice');

    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'image' => 'required|mimes:jpeg,bmp,png',
            'notice_name' => 'required|string',
        ]);
    }


    public function noticeAll(){
	    $noticeAllData=DB::select('select * from notice');
	   // var_dump($notciceAllData);
        return view('notice',compact('noticeAllData'));
    }

    public function showNotice($id)
    {
        $noticeData = DB::select('select * from notice where notice_id="' . $id . '"');
        return view('noticeindividual',compact('noticeData'));
    }

    }
