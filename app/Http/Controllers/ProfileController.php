<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

         $email = Auth::user()->email;

         $checker = Auth::user()->chk;



        if ($checker==0) {
            $i = 0;
            $teacher_datas = DB::select('select * from teacher where t_email="' . $email . '"');
           $name = Auth::user()->name;

            $data41as = DB::select('select * from fourone_as where teacher_name="' . $name . '"');

            foreach ($data41as as $data41a){

                $schedule_data[$i] = array('Day' => $data41a->day, 'Level' => 4, 'Term' => 1, 'Time' => $data41a->time, 'Section' => 'A', 'Course' => $data41a->sub_title);
                $i=$i+1;
             }

            $data41bs = DB::select('select * from fourone_bs where teacher_name="' . $name . '"');
            foreach ($data41bs as $data41b){

                $schedule_data[$i] = array('Day' => $data41b->day, 'Level' => 4, 'Term' => 1, 'Time' => $data41b->time, 'Section' => 'B', 'Course' => $data41b->sub_title);
                $i=$i+1;
            }

            $data42as = DB::select('select * from fourtwo_as where teacher_name="' . $name . '"');
            foreach ($data42as as $data42a){

                $schedule_data[$i] = array('Day' => $data42a->day, 'Level' => 4, 'Term' => 2, 'Time' => $data42a->time, 'Section' => 'A', 'Course' => $data42a->sub_title);
                $i=$i+1;
            }

            $data42bs = DB::select('select * from fourtwo_bs where teacher_name="' . $name . '"');
            foreach ($data42bs as $data42b){

                $schedule_data[$i] = array('Day' => $data42b->day, 'Level' => 4, 'Term' => 2, 'Time' => $data42b->time, 'Section' => 'B', 'Course' => $data42b->sub_title);
                $i=$i+1;
            }

            $data31as = DB::select('select * from threeone_as where teacher_name="' . $name . '"');
            foreach ($data31as as $data31a){

                $schedule_data[$i] = array('Day' => $data31a->day, 'Level' => 3, 'Term' => 1, 'Time' => $data31a->time, 'Section' => 'A', 'Course' => $data31a->sub_title);
                $i=$i+1;
            }

            $data31bs = DB::select('select * from threeone_bs where teacher_name="' . $name . '"');
            foreach ($data31bs as $data31b){

                $schedule_data[$i] = array('Day' => $data31b->day, 'Level' => 3, 'Term' => 1, 'Time' => $data31b->time, 'Section' => 'B', 'Course' => $data31b->sub_title);
                $i=$i+1;
            }

            $data32as = DB::select('select * from threetwo_as where teacher_name="' . $name . '"');
            foreach ($data32as as $data32a){

                $schedule_data[$i] = array('Day' => $data32a->day, 'Level' => 3, 'Term' => 2, 'Time' => $data32a->time, 'Section' => 'A', 'Course' => $data32a->sub_title);
                $i=$i+1;
            }

            $data32bs = DB::select('select * from threetwo_bs where teacher_name="' . $name . '"');
            foreach ($data32bs as $data32b){

                $schedule_data[$i] = array('Day' => $data32b->day, 'Level' => 3, 'Term' => 2, 'Time' => $data32b->time, 'Section' => 'B', 'Course' => $data32b->sub_title);
                $i=$i+1;
            }

            $data11as = DB::select('select * from oneoneas where teacher_name="' . $name . '"');
            foreach ($data11as as $data11a){

                $schedule_data[$i] = array('Day' => $data11a->day, 'Level' => 1, 'Term' => 1, 'Time' => $data11a->time, 'Section' => 'A', 'Course' => $data11a->sub_title);
                $i=$i+1;
            }

            $data11bs = DB::select('select * from oneonebs where teacher_name="' . $name . '"');
            foreach ($data11bs as $data11b){

                $schedule_data[$i] = array('Day' => $data11b->day, 'Level' => 1, 'Term' => 1, 'Time' => $data11b->time, 'Section' => 'B', 'Course' => $data11b->sub_title);
                $i=$i+1;
            }

            $data12as = DB::select('select * from onetwoas where teacher_name="' . $name . '"');
            foreach ($data12as as $data12a){

                $schedule_data[$i] = array('Day' => $data12a->day, 'Level' => 1, 'Term' => 2, 'Time' => $data12a->time, 'Section' => 'A', 'Course' => $data12a->sub_title);
                $i=$i+1;
            }

            $data12bs = DB::select('select * from onetwobs where teacher_name="' . $name . '"');
            foreach ($data12bs as $data12b){

                $schedule_data[$i] = array('Day' => $data12b->day, 'Level' => 1, 'Term' => 2, 'Time' => $data12b->time, 'Section' => 'B', 'Course' => $data12b->sub_title);
                $i=$i+1;
            }

            $data21as = DB::select('select * from twooneas where teacher_name="' . $name . '"');
            foreach ($data21as as $data21a){

                $schedule_data[$i] = array('Day' => $data21a->day, 'Level' => 2, 'Term' => 1, 'Time' => $data21a->time, 'Section' => 'A', 'Course' => $data21a->sub_title);
                $i=$i+1;
            }

            $data21bs = DB::select('select * from twoonebs where teacher_name="' . $name . '"');
            foreach ($data21bs as $data21b){

                $schedule_data[$i] = array('Day' => $data21b->day, 'Level' => 2, 'Term' => 1, 'Time' => $data21b->time, 'Section' => 'B', 'Course' => $data21b->sub_title);
                $i=$i+1;
            }

            $data22as = DB::select('select * from twotwoas where teacher_name="' . $name . '"');
            foreach ($data22as as $data22a){

                $schedule_data[$i] = array('Day' => $data22a->day, 'Level' => 2, 'Term' => 2, 'Time' => $data22a->time, 'Section' => 'A', 'Course' => $data22a->sub_title);
                $i=$i+1;
            }

            $data22bs = DB::select('select * from twotwobs where teacher_name="' . $name . '"');
            foreach ($data22bs as $data22b){

                $schedule_data[$i] = array('Day' => $data22b->day, 'Level' => 2, 'Term' => 2, 'Time' => $data22b->time, 'Section' => 'B', 'Course' => $data22b->sub_title);
                $i=$i+1;
            }
           // var_dump($data11bs);
            //var_dump($schedule_data);

            if(empty($schedule_data)){
                return view('sign_out', compact('teacher_datas'));
            }else {
                return view('sign_out', compact('schedule_data', 'teacher_datas'));
            }
        }elseif ($checker==1) {
            return view('sign_out1');
        }
        elseif ($checker==2) {
            return view('admin');
        }

    }
}
