<?php

namespace App\Http\Controllers;

use App\Research;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;


class ResearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function store()
    {
        $objResearch=new Research();
        $objResearch->author_name=$_POST['author_name'];
        $objResearch->paper_name=$_POST['paper_name'];
        $objResearch->type=$_POST['type'];
        $objResearch->journal_name=$_POST['journal_name'];
        $objResearch->publication_year=$_POST['publication_year'];
        $objResearch->t_id=$_POST['t_id'];
        $status=$objResearch->save();
        return redirect()->route('Research');
    }
    public function index(){
        $objResearch=new Research();
        $allData=$objResearch->paginate(5);
        return view("Research/index",compact('allData'));

    }


    public function create(){
        $email = Auth::user()->email;

        $checker = Auth::user()->chk;
        if($checker!=0)
            redirect('/');
        $teacherID=DB::select('select t_id from teacher where t_email="' . $email . '"');
        $data= $teacherID[0]->t_id;
        return view('Research/create',compact('data'));
    }

    public function view4Edit($id){
        $objResearch=new Research();
        $oneData=$objResearch->find($id);
        return view("Research/edit",compact('oneData'));
    }

    public function update(){
        $objResearch=new Research();
        $oneData=$objResearch->find($_POST['id']);
        $oneData->author_name=$_POST['author_name'];
        $oneData->paper_name=$_POST['paper_name'];
        $oneData->type=$_POST['type'];
        $oneData->journal_name=$_POST['journal_name'];
        $oneData->publication_year=$_POST['publication_year'];
        $status=$oneData->update();

        return redirect()->route('Research');
    }
    public function delete($id){
        $objResearch=new Research();
        $oneData=$objResearch->find($id)->delete();
        return redirect()->route('Research');
    }
}

