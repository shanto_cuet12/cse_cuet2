<?php

namespace App\Http\Controllers;

use App\FouroneB;
use Illuminate\Http\Request;

class FouroneBcontroller extends Controller
{
    public function store(){
        $objFouroneB=new FouroneB();
        $objFouroneB->sub_title=$_POST['sub_title'];
        $objFouroneB->teacher_name=$_POST['teacher_name'];
        $objFouroneB->day=$_POST['day'];
        $objFouroneB->time=$_POST['time'];
        $status=$objFouroneB->save();
        return redirect()->route('FouroneBroutine');
    }
    public function index(){
        $objFouroneB=new FouroneB();
        $allData=$objFouroneB->paginate(20);
        return view("FouroneB/index",compact('allData'));
    }
    public function index1(){
        $objFouroneB=new FouroneB();
        $allData=$objFouroneB->paginate(20);
        return view("FouroneB/index1",compact('allData'));
    }

    public function view4Edit($id){
        $objFouroneB=new FouroneB();
        $oneData=$objFouroneB->find($id);
        return view("FouroneB/edit",compact('oneData'));
    }

    public function update(){
        $objFouroneB=new FouroneB();
        $oneData=$objFouroneB->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('FouroneBroutine');
    }

    public function delete($id){
        $objFouroneB=new FouroneB();
        $oneData=$objFouroneB->find($id)->delete();
        return redirect()->route('FouroneBroutine');
    }

}
