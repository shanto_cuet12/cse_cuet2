<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Teacher;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;



class admin extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function teacherRegsForm(){
        if(Auth::user()->chk!=2){
            redirect('/home1');
        }
        else return view('Teacher/insert');
    }

    public function teacherRegs(Request $request){
        $this->validator($request->all());
	$image =file_get_contents($request->file('image'));
        $this->create($request->all(),$image);
        return redirect('sign_out');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            't_name' => 'required|string|max:255',
            't_email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data,$image)
    {	
		 
		
		Teacher::create([
		't_name'=>$data['t_name'],
		't_email'=>$data['t_email'],
		't_image'=>base64_encode($image),
		't_designation'=>$data['t_designation'],
		'b_name'=>$data['b_name'],
		'contact'=>$data['contact'],
		'r_interest'=>$data['r_interest'],
		'b_sub'=>$data['b_sub'],
		'b_uni'=>$data['b_uni'],
		'b_country'=>$data['b_country'],
		'm_sub'=>$data['m_sub'],
		'm_uni'=>$data['m_uni'],
		'm_country'=>$data['m_country'],
		'p_sub'=>$data['p_sub'],
		'p_uni'=>$data['p_uni'],
		]);
        return User::create([
            'name' => $data['t_name'],
            'email' => $data['t_email'],
            'password' => bcrypt($data['password']),
            'chk' =>0,
        ]);
    }
}
