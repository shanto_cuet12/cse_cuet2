<?php

namespace App\Http\Controllers;

use App\Twotwoa;
use Illuminate\Http\Request;

class TwotwoaController extends Controller
{
    public function store(){
        $objTwotwoa=new Twotwoa();
        $objTwotwoa->sub_title=$_POST['sub_title'];
        $objTwotwoa->teacher_name=$_POST['teacher_name'];
        $objTwotwoa->day=$_POST['day'];
        $objTwotwoa->time=$_POST['time'];
        $status=$objTwotwoa->save();
        return redirect()->route('Twotwoaroutine');
    }
    public function index(){
        $objTwotwoa=new Twotwoa();
        $allData=$objTwotwoa->paginate(20);
        return view("Twotwoa/index",compact('allData'));
    }
    public function index1(){
        $objTwotwoa=new Twotwoa();
        $allData=$objTwotwoa->paginate(20);
        return view("Twotwoa/index1",compact('allData'));
    }
    public function view4Edit($id){
        $objTwotwoa=new Twotwoa();
        $oneData=$objTwotwoa->find($id);
        return view("Twotwoa/edit",compact('oneData'));
    }

    public function update(){
        $objTwotwoa=new Twotwoa();
        $oneData=$objTwotwoa->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('Twotwoaroutine');
    }
    public function delete($id){
        $objTwotwoa=new Twotwoa();
        $oneData=$objTwotwoa->find($id)->delete();
        return redirect()->route('Twotwoaroutine');
    }
}
