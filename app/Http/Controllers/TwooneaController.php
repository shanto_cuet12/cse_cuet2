<?php

namespace App\Http\Controllers;

use App\Twoonea;
use Illuminate\Http\Request;

class TwooneaController extends Controller
{
    public function store(){
        $objTwoonea=new Twoonea();
        $objTwoonea->sub_title=$_POST['sub_title'];
        $objTwoonea->teacher_name=$_POST['teacher_name'];
        $objTwoonea->day=$_POST['day'];
        $objTwoonea->time=$_POST['time'];
        $status=$objTwoonea->save();
        return redirect()->route('Twoonearoutine');
    }
    public function index(){
        $objTwoonea=new Twoonea();
        $allData=$objTwoonea->paginate(20);
        return view("Twoonea/index",compact('allData'));
    }
    public function index1(){
        $objTwoonea=new Twoonea();
        $allData=$objTwoonea->paginate(20);
        return view("Twoonea/index1",compact('allData'));
    }
    public function view4Edit($id){
        $objTwoonea=new Twoonea();
        $oneData=$objTwoonea->find($id);
        return view("Twoonea/edit",compact('oneData'));
    }

    public function update(){
        $objTwoonea=new Twoonea();
        $oneData=$objTwoonea->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('Twoonearoutine');
    }
    public function delete($id){
        $objTwoonea=new Twoonea();
        $oneData=$objTwoonea->find($id)->delete();
        return redirect()->route('Twoonearoutine');
    }
}
