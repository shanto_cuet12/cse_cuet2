<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class facultyController extends Controller
{
    public function index(){
		$datas=DB::select('select * from teacher');
        $h=0;
        $p=0;
        $ascp=0;
        $assp=0;
        $lec=0;
		foreach($datas as $data){
			if($data->t_name=='Dr. MD Samsul Arefin'){
				 $retrivedData['head'][$h]=array('name'=>$data->t_name,'Designation'=>$data->t_designation,'id'=>$data->t_id,'image'=>$data->t_image);
				
			}
			
			if($data->t_designation=='Professor'){
				 $retrivedData['Professor'][$p]=array('name'=>$data->t_name,'Designation'=>$data->t_designation,'id'=>$data->t_id,'image'=>$data->t_image);
				 $p=$p+1;
				
			}
			
			if($data->t_designation=='Assistant Professor'){
				 $retrivedData['AsstProfessor'][$assp]=array('name'=>$data->t_name,'Designation'=>$data->t_designation,'id'=>$data->t_id,'image'=>$data->t_image);
				 $assp=$assp+1;
				
			}
			
			if($data->t_designation=='Associate Professor'){
				 $retrivedData['AsscProfessor'][$ascp]=array('name'=>$data->t_name,'Designation'=>$data->t_designation,'id'=>$data->t_id,'image'=>$data->t_image);
				$ascp=$ascp+1;
			}
			
			if($data->t_designation=='Lecturer'){
				 $retrivedData['Lecturer'][$p]=array('name'=>$data->t_name,'Designation'=>$data->t_designation,'id'=>$data->t_id,'image'=>$data->t_image);
				$lec=$lec+1;
			}
	
		}

		return view('faculties',compact('retrivedData'));
	}
	
	public function showFaculty($id) {
		$facultyData=DB::select('select * from teacher where t_id="'.$id.'"');
		$infos=DB::select('select course_name,cat from related_courses_links rcl join related_courses rc on rc.course_id=rcl.course_id where rcl.t_id="'.$id.'"');
		$ug=0;
		$pg=0;
		$jrnl=0;
		$conf=0;
		foreach($infos as $info){
			
			if($info->cat=='Undergraduate') {
					$retInfo['ug'][$ug]=array('courseName'=>$info->course_name);
					$ug=$ug+1;
				}
				
			if($info->cat=='Postgraduate') {
					$retInfo['pg'][$pg]=array('courseName'=>$info->course_name);
					$pg=$pg+1;
				}
		}
			
		$researchDatas = DB::select('select * from researches where t_id="'.$id.'"');
		
		foreach($researchDatas as $researchData){
			
			if($researchData->type=='journal') {
					$retInfo['journal'][$jrnl]=array('author_name'=>$researchData->author_name,'paper_name'=>$researchData->paper_name,'journal_name'=>$researchData->journal_name,'publication_year'=>$researchData->publication_year);
					$jrnl=$jrnl+1;
				}
				
			if($researchData->type=='conference') {
					$retInfo['conference'][$conf]=array('author_name'=>$researchData->author_name,'paper_name'=>$researchData->paper_name,'journal_name'=>$researchData->journal_name,'publication_year'=>$researchData->publication_year);
					$conf=$conf+1;
				}
				

				
			}		
			
		
		if(empty($retInfo)) {
		return view('facultyProfile',compact('facultyData'));
		}else {
			return view('facultyProfile',compact('retInfo','facultyData'));		
		}
		
	}

}
