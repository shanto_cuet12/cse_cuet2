<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Research extends Model
{
    public $timestamps=false;
    protected $fillable = [
        'author_name', 'paper_name', 'type','journal_name','publication_year',
    ];
}
