<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
	protected $table = 'teacher';
    public $timestamps=false;
    protected $fillable = [
       't_name','t_email','t_image','t_designation','b_name','contact','r_interest','b_sub','b_uni','b_country','m_sub','m_uni','m_country','p_sub','p_uni',
    ];
}
