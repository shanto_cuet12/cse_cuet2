<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oneone extends Model
{
	protected $table = 'oneoneas';
    public $timestamps=false;
    protected $fillable = [
        'sub_title', 'teacher_name', 'day','time',
    ];
}
