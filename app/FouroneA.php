<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FouroneA extends Model
{
    public $timestamps=false;
    protected $fillable = [
        'sub_title', 'teacher_name', 'day','time',
    ];
}
